source docker-utils.sh

from_alpine                                                # FROM alpine
new_layer                                                  # RUN apk update && apk add python3
python3 lid.py "apk update"                                #
python3 lid.py "apk add python3"                           #
new_layer                                                  # COPY app.py /
cat << EOF > /tmp/app.py
#!/usr/bin/env python3
print("Hello world")
EOF
mv /tmp/app.py rootfs                                      #
python3 lid.py "chmod +x app.py"                           #
commit_image "./app.py" z2d                                # CMD ["./app.py"]
