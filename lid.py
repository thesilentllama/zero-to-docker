import ctypes
import os
import os.path


# Defined by the kernel in include/linux/sched.h:
CLONE_NEWNS     = 0x00020000  # New mount namespace group
CLONE_NEWCGROUP = 0x02000000  # New cgroup namespace (4.6+ kernels only)
CLONE_NEWUTS    = 0x04000000  # New utsname namespace
CLONE_NEWIPC    = 0x08000000  # New ipc namespace
CLONE_NEWUSER   = 0x10000000  # New user namespace
CLONE_NEWPID    = 0x20000000  # New pid namespace
CLONE_NEWNET    = 0x40000000  # New network namespace

cdll = ctypes.CDLL("libc.so.6")


if __name__ == "__main__":
    uid = os.getuid()
    gid = os.getgid()

    cdll.unshare(CLONE_NEWNS | CLONE_NEWCGROUP | CLONE_NEWUTS | CLONE_NEWIPC | CLONE_NEWUSER | CLONE_NEWPID)

    # Uncomment to create a separate network namespace:
    #cdll.unshare(CLONE_NEWNET)

    with open("/proc/self/setgroups", "w") as setgroups:
        setgroups.write("deny")

    with open("/proc/self/uid_map", "w") as uid_map:
        uid_map.write("0 {} 1\n".format(uid))

    with open("/proc/self/gid_map", "w") as gid_map:
        gid_map.write("0 {} 1\n".format(gid))

    pid = os.fork()

    if pid == 0:
        # Linux has another system call for this: pivot_root. It's not included in glibc
        # which is why we're not using it but it's more secure. It should be used in a production
        # implementation 
        os.chroot("rootfs")
        os.chdir("/")

        os.system("mount -t proc proc proc")

        if len(sys.argv) > 1:
            os.execve("/bin/sh", ["/bin/sh", "-c"] + sys.arg[1:], {"PATH": "/bin:/sbin:/usr/bin"})
        else:
            os.execve("/bin/sh", ["/bin/sh"], {"PATH": "/bin:/sbin:/usr/bin"})
    else:
        os.waitpid(pid, 0)
        
