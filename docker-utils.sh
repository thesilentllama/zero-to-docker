#!/bin/sh

ALPINE_TAR=alpine-minirootfs-3.13.0-x86_64.tar.gz
ALPINE_URL=https://dl-cdn.alpinelinux.org/alpine/v3.13/releases/x86_64/$ALPINE_TAR
LAYER=0
TMP=$(mktemp -d)

from_alpine() {
    cp lid.py $TMP
    cp template.json $TMP
    cd $TMP

    wget $ALPINE_URL
    mkdir rootfs0
    tar -C rootfs0 -xf $ALPINE_TAR
    echo "nameserver 1.1.1.1" > rootfs0/etc/resolv.conf
    ln -s rootfs0 rootfs
}

new_layer() {
    PREV=$LAYER
    LAYER=$((LAYER + 1))

    CURRENT_ROOTFS=rootfs$PREV
    NEW_ROOTFS=rootfs$LAYER
    WORK=work$LAYER
    CURRENT_LAYER=layer$LAYER

    mkdir $WORK
    mkdir $CURRENT_LAYER
    mkdir $NEW_ROOTFS

    unlink rootfs
    ln -s $NEW_ROOTFS rootfs

    mount -t overlay overlay -o lowerdir=$CURRENT_ROOTFS,upperdir=$CURRENT_LAYER,workdir=$WORK $NEW_ROOTFS
}

commit_image() {
    L=$LAYER

    # Unmount layers in reverse order:
    while [ $L -gt 0 ]
    do
        umount rootfs$L
        L=$((L - 1))
    done
    
    mkdir image
    
    tar -C rootfs0 -cf layer0.tar .
    HASH=$(sha256sum layer0.tar | cut -d " " -f1)
    echo "Creating layer $HASH"
    mkdir image/$HASH
    mv layer0.tar image/$HASH/layer.tar
    echo "1.0" > image/$HASH/VERSION
    cat template.json | jq ". + {id: \"$HASH\"}" > image/$HASH/json
    
    PARENT=$HASH

    # Create tarball layers in order:
    L=1
    while [ $L -le $LAYER ]
    do
        tar -C layer$L -cf layer$L.tar .

        HASH=$(sha256sum layer$L.tar | cut -d " " -f1)
        echo "Creating layer $HASH"
        mkdir image/$HASH
        mv layer$L.tar image/$HASH/layer.tar
        echo "1.0" > image/$HASH/VERSION
        cat template.json | jq ". + {id: \"$HASH\"}" | jq ". + {parent: \"$PARENT\"}" | jq ".config.Cmd = [\"$1\"]" > image/$HASH/json
        
        PARENT=$HASH
        L=$((L + 1))
    done

    echo "{}" | jq ". + {$2: {}}" | jq ". .$2+={latest: \"$PARENT\"}" > image/repositories

    # Create the final docker image
    tar -C image -cf image.tar .
    mv image.tar /

    cd /
    rm -rf $TMP
}
