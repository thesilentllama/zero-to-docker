#!/bin/sh

wget https://dl-cdn.alpinelinux.org/alpine/v3.13/releases/x86_64/alpine-minirootfs-3.13.0-x86_64.tar.gz
mkdir rootfs;
tar -C rootfs -xf alpine-minirootfs-3.13.0-x86_64.tar.gz
echo "nameserver 1.1.1.1" > rootfs/etc/resolv.conf
